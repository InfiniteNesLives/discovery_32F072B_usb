
#Build directory
BUILD = build

#project name
#doesn't need to be associated with any file names
PROJ = usb_demo


# Selecting Core
CORTEX_M=0

# Use newlib-nano. To disable it, specify USE_NANO=
#USE_NANO=--specs=nano.specs
USE_NANO=

# Use seimhosting or not
USE_SEMIHOST=--specs=rdimon.specs
USE_NOHOST=--specs=nosys.specs

CORE=CM$(CORTEX_M)
BASE=.

# Compiler & Linker
CC=arm-none-eabi-gcc
CXX=arm-none-eabi-g++
OBJCOPY=arm-none-eabi-objcopy
SIZE=arm-none-eabi-size

# Options for specific architecture
ARCH_FLAGS=-mthumb -mcpu=cortex-m$(CORTEX_M)

# Startup code
STARTUP=$(BASE)/startup/startup_ARM$(CORE).S

# -Os -flto -ffunction-sections -fdata-sections to compile for code size
CFLAGS=$(ARCH_FLAGS) $(STARTUP_DEFS) -Os -flto -ffunction-sections -fdata-sections -g
CXXFLAGS=$(CFLAGS)

# Link for code size
GC=-Wl,--gc-sections

# Create map file
MAP=-Wl,-Map=$(BUILD)/$(PROJ).map

STARTUP_DEFS=-D__STARTUP_CLEAR_BSS -D__START=main -D__NO_SYSTEM_INIT

LDSCRIPTS=-L. -L$(BASE)/ldscripts -T nokeep.ld
LFLAGS=$(USE_NANO) $(USE_NOHOST) $(LDSCRIPTS) $(GC) $(MAP)

DEFINE+=\
	-DSTM32F070xB \
	-DF_CPU=16000000
#128KB version of all packages (LQFP-48,64,100)

#	-DSTM32F072x8 \ #64KB version of all packages (LQFP-48,64,100)
#	-DSTM32F070xB \ #128KB version of both packages (LQFP-48,64)
#	-DSTM32F070x6 \ #32KB version of both packages (TSSOP-20,LQFP-48)
#	-DF_CPU=8000000
INCLUDE=-I ./include
CFLAGS+= $(DEFINE) $(INCLUDE) 

SOURCES=$(wildcard source/**/*.c source/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))

#all: dir $(BUILD)/$(PROJ).axf $(BUILD)/$(PROJ).elf $(BUILD)/$(PROJ).hex $(BUILD)/$(PROJ).bin size 
all: dir $(BUILD)/$(PROJ).elf $(BUILD)/$(PROJ).hex $(BUILD)/$(PROJ).bin size 

#build axf file output (basically elf with DWARF debug info)
# $@ is shortcut for the target, $^ is shortcut for prereqs
#             TARGET: PREREQS
$(BUILD)/$(PROJ).axf: $(STARTUP) $(OBJECTS)
	$(CC) $^ $(CFLAGS) $(LFLAGS) -o $@

$(BUILD)/$(PROJ).elf: $(STARTUP) $(OBJECTS)
	$(CC) $^ $(CFLAGS) $(LFLAGS) -o $@

$(BUILD)/$(PROJ).hex: $(BUILD)/$(PROJ).elf
	$(OBJCOPY) -O ihex $^ $@

$(BUILD)/$(PROJ).bin: $(BUILD)/$(PROJ).elf
	$(OBJCOPY) -O binary $^ $@

dir:
	mkdir -p $(BUILD)

size:	$(BUILD)/$(PROJ).elf
	$(SIZE) -t $^

program: all
	ST-LINK_CLI.exe -c -P $(BUILD)\$(PROJ).hex 0x08000000 -Rst

disassm: all
	arm-none-eabi-objdump $(BUILD)\$(PROJ).elf -d -g

clean: 
	rm -rf $(BUILD)
	rm -f $(OBJECTS)
