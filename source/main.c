
static int log = 0;

#ifndef __NO_SYSTEM_INIT
//option to create your own C library system initialization
//Currently define __STARTUP_CLEAR_BSS when building to only use
//initialization provided by startup_ARMCM0.S that zero's the BSS section
//To get the function below called in between reset routine and main function,
//remove definition from makefile
void SystemInit()
{
//	TODO Some processor initializations might be a in good order.
//	currently just sticking with default main stack pointer (MSP)
//	process SP is unused..
}
#endif

//include target chip port definition library files
#include <stm32f0xx.h>
#include <stm32f0xx_rcc.h>
#include <stm32f0xx_gpio.h>

//include target board library files
//this is junk... #include <stm32f072b_discovery.h>

//kaz6 is PB1
//#define LED	(1U)
//#define IOP_LED_EN IOPBEN
//#define GPIO_LED   GPIOB

//kaz adapter is PC13
#define LED	(13U)
#define IOP_LED_EN IOPCEN
#define GPIO_LED   GPIOC

//kaz adapter data0 debug is PB8
#define DEBUG	(8U)
#define IOP_DEBUG_EN IOPBEN
#define GPIO_DEBUG   GPIOB

#define LED_ON() (GPIO_LED->ODR |= (0x1U<<LED))
#define LED_OFF() (GPIO_LED->ODR &= ~(0x1U<<LED))

#define DEBUG_HI() (GPIO_DEBUG->ODR |= (0x1U<<DEBUG))
#define DEBUG_LO() (GPIO_DEBUG->ODR &= ~(0x1U<<DEBUG))

//Here's where the LEDs and switch are located
//PA0 user switch
//#define SWITCH	(0U)

//PC6 RED/UP
//#define RED	(6U)
//#define LED	(1U)	//kaz6 has LED on PB1
//#define LED	(13U)	//kaz adapter has LED on PC13
//#define LED_ON() GPIOC->ODR |= 1<<LED
//#define LED_OFF() GPIOC->ODR &= ~(1<<LED)
//PC7 DOWN/BLUE
//#define BLUE	(7U)
////PC8 LEFT/YELLOW
//#define YELLOW	(8U)
////PC9 RIGHT/GREEN
//#define GREEN	(9U)

//RCC->AHBENR AHB clock enable register
//set bit to enable clock
#define	DMAEN	(0U)	//DMA clock
#define	DMA2EN	(1U)	//DMA2 clock
#define	SRAMEN	(2U)	//SRAM interface during sleep
#define	FLITFEN	(4U)	//Flash interface during sleep
#define	CRCEN	(6U)	//CRC clock
#define	IOPAEN	(17U)	//IO port A
#define	IOPBEN	(18U)	//IO port B
#define	IOPCEN	(19U)	//IO port C
#define	IOPDEN	(20U)	//IO port D
#define	IOPEEN	(21U)	//IO port E
#define	IOPFEN	(22U)	//IO port F
#define	TSCEN	(24U)	//touch sensing controller

//clear RX interrupt
//set tx field to keep from accidentally clearing //mask out toggle feilds making them zero //clear rx bit removing active interrupt
#define EP0R_CLR_CTR_RX()	 USB->EP0R = (((USB->EP0R | USB_EP_CTR_TX) 	& USB_EPREG_MASK ) 	& ~USB_EP_CTR_RX )

//clear TX interrupt
#define EP0R_CLR_CTR_TX()	 USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX) 	& USB_EPREG_MASK ) 	& ~USB_EP_CTR_TX )

//VALID need to get both status bits set
//XOR the current value of status bits with 1 to write back inverted value, gets all status bits set
//							OR in bits that need set,	AND in bits to keep or avail for XOR,  XOR toggles to invert
#define	USB_EP0R_RXTX_VALID() USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & ~(USB_EP_DTOG_RX | USB_EP_DTOG_TX)) ^ (USB_EPTX_STAT | USB_EPRX_STAT))	
#define	USB_EP0R_RX_VALID() USB->EP0R = ((((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX)) & (USB_EPREG_MASK | USB_EPRX_STAT)) ^ USB_EPRX_STAT )
#define	USB_EP0R_TX_VALID() USB->EP0R = ((((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX)) & (USB_EPREG_MASK | USB_EPTX_STAT)) ^ USB_EPTX_STAT )

//DISABLE need to get both bits cleared
//write back current value of status bits to toggle all 1's to zeros
//							OR in bits that need set	  AND in bits to keep or toggle from 1 to 0
#define	USB_EP0R_RXTX_DIS() USB->EP0R = ((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & ~(USB_EP_DTOG_RX | USB_EP_DTOG_TX))
#define	USB_EP0R_RX_DIS() USB->EP0R = ((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & (USB_EPREG_MASK | USB_EPRX_STAT))
#define	USB_EP0R_TX_DIS() USB->EP0R = ((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & (USB_EPREG_MASK | USB_EPTX_STAT))

//NAK/STALL need to get one bit set, and the other cleared
//Easiest way would be to DISABLE, and then set desired bit, uses two accesses to EP0R
//			  DISABLE first			              OR in bits that need set		AND in bits to keep or toggle
#define USB_EP0R_RX_NAK() USB_EP0R_RX_DIS(); USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & (USB_EPREG_MASK)) | USB_EP_RX_NAK)
#define USB_EP0R_TX_NAK() USB_EP0R_TX_DIS(); USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & (USB_EPREG_MASK)) | USB_EP_TX_NAK)
#define USB_EP0R_RXTX_NAK() USB_EP0R_RXTX_DIS(); USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & (USB_EPREG_MASK)) | (USB_EP_RX_NAK | USB_EP_TX_NAK))

#define USB_EP0R_RX_STALL() USB_EP0R_RX_DIS(); USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & (USB_EPREG_MASK)) | USB_EP_RX_STALL)
#define USB_EP0R_TX_STALL() USB_EP0R_TX_DIS(); USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & (USB_EPREG_MASK)) | USB_EP_TX_STALL)
#define USB_EP0R_RXTX_STALL() USB_EP0R_RXTX_DIS(); USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) & (USB_EPREG_MASK)) | (USB_EP_RX_STALL | USB_EP_TX_STALL))


//set bit(s) that would like to toggle
			//set rx/tx field to keep from accidentally clearing
		//mask out toggle feilds making them zero
//TODO fix assumption that we only need to go from TX_NAK to TX_VALID

void HardFault_Handler(void)
{

	//TODO test out this function
	//should retrieve PC of the instruction that follows whatever caused the hardfault
	//This didn't work for me earlier bc the stack itself was broke
//	asm(
//	"movs r0, #4	\n"
//	"movs r1, lr	\n"
//	"tst r0, r1	\n"
//	"beq _MSP	\n"
//	"mrs r0, psp	\n"
//	"b _HALT	\n"
//	"_MSP:		\n"
//	"	mrs r0, msp	\n"
//	"_HALT:	\n"
//	"	ldr r1, [r0,#20]	\n"
//	//"	bkpt #0	\n"
//	);

	while (1) {
	}
}


/* stm32f0x2 devices have HSI48 available which isn't on stm32f0x0 devices
 * I'm primarily targetting f070 devices which will require external xtal
 * Most generic setup would be to use HSI 8MHz * PLL which is available on all devices
 * but can't keep all devices running at 16Mhz while also properly clocking usb with that setup.
 *
 * After reset all devices select HSI 8MHz as SYSCLK
 * To prevent usb over/under run problems, APB clock must be atleast 10Mhz
 * 070 can only use PLL to feed USB clock.
 *
 * Current goal is to have PLL output 48Mhz from a 16Mhz HSE to support 070 devices
 * 072 devices won't have ext xtal though, and use HSI 48Mhz for usb block
 * Would like to have SYSCLK = 16Mhz to align stm32 and avr kazzo devices core clocks for now
 * Have to also supply APB with 10Mhz or more, which is derived from SYSCLK with AHB & APB dividers
 *
 * While these goals are possible, we have to code them based on 072/070 devices
 * 072: HSI 8Mhz -> PLL * 2 = 16Mhz -> SYSCLK -> no division to AHB & APB clocks
 * 	HSI 48Mhz -> USB block
 * 070: HSE 16Mhz -> PLL * 4 = 48Mhz -> USB block
 * 	HSE 16Mhz -> SYSCLK -> no division to AHB & APB clocks
 *
 * 	Difference between these two is the PLL.  
 * 	072 uses PLL to create 16Mhz SYSCLK from HSI, USB fed directly from HSI 48Mhz
 * 		-HSE not available/used on 072
 * 	070 uses PLL to create 48Mhz for USB from HSE, SYSCLK fed directly from 16Mhz HSE.
 * 		-HSI not used for anything on 070
 */

//pick define based on xtal setup for init_clock and init_usb_clock functions
//#define NO_XTAL
#define XTAL_16Mhz
void init_clock()
{
#ifdef NO_XTAL // setup PLL for HSI * 2 = 16Mhz and set SYSCLK to use it
	
	// To modify the PLL configuration, proceed as follows:
	// 1.Disable the PLL by setting PLLON to 0.
	// 2. Wait until PLLRDY is cleared. The PLL is now fully stopped.
	// * PLL is off and unlocked after reset
	
	// 3. Change the desired parameter.
	// * PLL MUL is set to *2 at reset

	// 4. Enable the PLL again by setting PLLON to 1.
	// 5. Wait until PLLRDY is set.
	
//Copied from reference manual optimizations possible assuming post-reset
//Cut out parts not needed due to values @ reset, saved 60Bytes!
//	/* (1) Test if PLL is used as System clock */
//	if ((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL) {
//		RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW); /* (2) Select HSI as system clock */
//		while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI) /* (3) Wait for HSI switched */
//		{ /* For robust implementation, add here time-out management */ }
//	}
//
//	RCC->CR &= (uint32_t)(~RCC_CR_PLLON);/* (4) Disable the PLL */
//	while((RCC->CR & RCC_CR_PLLRDY) != 0) /* (5) Wait until PLLRDY is cleared */
//	{ /* For robust implementation, add here time-out management */ }
//
//	/* (6) Set the PLL multiplier to 2-16 all integers */
//	RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_PLLMUL) | RCC_CFGR_PLLMUL2; /* PLLMUL set to *2 at reset) */

//	// PLL PREDIV should be getting set to zero too!  They just assumed not present/reset values...
//	RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_PLLSRC) | RCC_CFGR_PLLSRC_HSI_PREDIV; /* PLLMUL set to *2 at reset) */

//	// They also didn't address flash wait states!
//	FLASH->ACR |= (uint32_t) 0x01;	//If >24Mhz SYSCLK, must add wait state to flash

//	PREDIV is / 2 post reset, so PLL is being sourced with 4Mhz
	RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_PLLMUL) | RCC_CFGR_PLLMUL4; /* PLLMUL set to *2 at reset) */
	RCC->CR |= RCC_CR_PLLON; /* (7) Enable the PLL */
	while((RCC->CR & RCC_CR_PLLRDY) == 0) /* (8) Wait until PLLRDY is set */
	{ /* For robust implementation, add here time-out management */ }

	RCC->CFGR |= (uint32_t) (RCC_CFGR_SW_PLL); /* (9) Select PLL as system clock */
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL) /* (10) Wait until the PLL is switched on */
	{ /* For robust implementation, add here time-out management */ }

#endif
	

#ifdef XTAL_16Mhz

	//Turn on HSE
	/* (2) Enable the CSS
	 * Enable the HSE and set HSEBYP to use the internal clock
	 * Enable HSE */
	RCC->CR |= (RCC_CR_CSSON | RCC_CR_HSEON); /* (2) */

	/* (1) Check the flag HSE ready */
	while ((RCC->CR & RCC_CR_HSERDY) == 0) /* (1) */
	{ /*spin while waiting for HSE to be ready */	}


	/* (3) Switch the system clock to HSE */
	//at startup HSI is selected SW = 00
	RCC->CFGR |= RCC_CFGR_SW_HSE;

	//Now the SYSCLK is running directly off the HSE 16Mhz xtal

	/* (1) Test if PLL is used as System clock */
	if ((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL) {
		RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW); /* (2) Select HSI as system clock */
		while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI) /* (3) Wait for HSI switched */
		{ /* For robust implementation, add here time-out management */ }
	}

	RCC->CR &= (uint32_t)(~RCC_CR_PLLON);/* (4) Disable the PLL */
	while((RCC->CR & RCC_CR_PLLRDY) != 0) /* (5) Wait until PLLRDY is cleared */
	{ /* For robust implementation, add here time-out management */ }

	//Set PLL Source to HSE, the PLL must be off to do this
	RCC->CFGR |= RCC_CFGR_PLLSRC_HSE_PREDIV;	//by default HSE isn't divided
	
	//Set PLL to 16 * 3 = 48Mhz for USB
	//RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_PLLMUL) | RCC_CFGR_PLLMUL3; /* PLLMUL set to *2 at reset) */
	RCC->CFGR |= RCC_CFGR_PLLMUL3; /* PLLMUL set to *2 at reset) */
	RCC->CR |= RCC_CR_PLLON; /* (7) Enable the PLL */
	while((RCC->CR & RCC_CR_PLLRDY) == 0) /* (8) Wait until PLLRDY is set */
	{ /* For robust implementation, add here time-out management */ }

	//test SYSCLK with 48Mhz
//	FLASH->ACR |= (uint32_t) 0x01;	//If >24Mhz SYSCLK, must add wait state to flash
//	RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_SW) | RCC_CFGR_SW_PLL; /* (9) Select PLL as system clock */
//	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL) /* (10) Wait until the PLL is switched on */
//	{ /* For robust implementation, add here time-out management */ }

#endif

	// The USB peripheral logic uses a dedicated clock. The frequency of this
	// dedicated clock is fixed by the requirements of the USB standard at 48 MHz, 
	// and this can be different from the clock used for the interface to the APB bus. 
	// Different clock configurations are possible where the APB clock frequency can be higher or lower than the USB peripheral
	// one.
	// Due to USB data rate and packet memory interface requirements, 
	// the APB clock must have a minimum frequency of 10 MHz to avoid data overrun/underrun problems.
	
	//AHB APB clock setup:
	//these are not divided by default
}

void init_usb_clock()
{
	// stm32f0x2 devices have HSI 48Mhz available to clock usb block, or PLL if it's source accurate enough
	// stm32f0x0 devices must have ext xtal and use PLL output to drive usb block
	
#ifdef XTAL_16Mhz
	//by default the 072 has HSI 48Mhz selected as USB clock
	//on the 070 this equates to off, so 070 must set USBSW bit
	RCC->CFGR3 |= RCC_CFGR3_USBSW_PLLCLK;
#endif

#ifdef NO_XTAL
	//Turn on HSI48 supposedly it will turn itself on if USB is enabled with HSI48 selected as clock
	RCC->CR2 |= RCC_CR2_HSI48ON;

	while ((RCC->CR2 & RCC_CR2_HSI48RDY) != RCC_CR2_HSI48RDY) /* (10) Wait until the HSI48 is stable */
	{ /* For robust implementation, add here time-out management */ }

	//by default the 072 has HSI 48Mhz selected as USB clock
	RCC->CFGR3 &= ~RCC_CFGR3_USBSW_Msk;
	//on the 070 this equates to off, so 070 must set USBSW bit
	//CRS system must be turned on to keep HSI 48Mhz calibrated
	RCC->APB1ENR |= RCC_APB1ENR_CRSEN;
	//Default settings are good using SOF packets for calibration
#endif

	//enable USB block by providing clock
	RCC->APB1ENR |= RCC_APB1ENR_USBEN;


}


void usb_reset_recovery(){

//	USB->CNTR |= USB_CNTR_FRES;
//	USB->CNTR &= ~USB_CNTR_FRES;
	
	//Endpoint-specific registers
	//The number of these registers varies according to the number of endpoints that the USB peripheral is designed to handle. 
	//The USB peripheral supports up to 8 bidirectional endpoints. Each USB device must support a control endpoint whose 
	//address (EA bits) must be set to 0. The USB peripheral behaves in an undefined way if multiple endpoints 
	//are enabled having the same endpoint number value. For each endpoint, an USB_EPnR register is available to store 
	//the endpoint specific information.
	
	//Enable the USB device and set address to zero
	//USB device address (USB_DADDR)
	//bit7 must be set to enable USB functionality, bits 6:0 are the address, must be 0 prior to enumeration
	USB->DADDR = (uint16_t) USB_DADDR_EF;

	//They are also reset when an USB reset is received from the USB bus or forced through bit FRES in the CTLR register, 
	//except the CTR_RX and CTR_TX bits, which are kept unchanged to avoid missing a correct packet notification 
	//immediately followed by an USB reset event. Each endpoint has its USB_EPnR register where n is the endpoint identifier.
	//Read-modify-write cycles on these registers should be avoided because between the read and the write operations 
	//some bits could be set by the hardware and the next write would modify them before the CPU has the time to detect the change. 
	//For this purpose, all bits affected by this problem have an "invariant" value that must be used whenever their 
	//modification is not required. It is recommended to modify these registers with a load instruction where all the bits, 
	//which can be modified only by the hardware, are written with their "invariant" value.
	//
	//USB endpoint n register (USB_EPnR), n=[0..7]
	//Address offset: 0x00 to 0x1C
	//Reset value: 0x0000
	//
	//Bit 15 CTR_RX: Correct Transfer for reception
	//	Hardware sets on successful completetion of OUT/SETUP transaction
	//	a failed transaction won't set this bit
	//	Software can only clear this bit
	//	SETUP bit11 signifies if OUT/SETUP was received
	// USE: read this bit after interrupt to determine/verify interrupt was due to success
	//	service OUT/SETUP packet then clear the bit so subsequent interrupt can be detected properly
	// write 0 to clear, write 1 to leave as-is
	//
	//Bit 14 DTOG_RX: Data Toggle, for reception transfers
	//	For non-isosync xfrs this bit contains Data0/1 expectation of next data packet
	//	Hardware toggles this bit after ACK'ing the host from successful PID match
	//	For control EPs HW clears this bit after receiving SETUP PID
	//	For double buffered, or isosync EPs this bit has other meanings
	//	Software can toggle this value by writting a 1, writting 0 has no effect. 
	//	-This is manditory for non-control EPs!
	// USE: For control endpoints: Seems this bit can be used to determine if upcoming xfr is DATA0/1
	// 	Shouldn't have to do/care much with this bit for control endpoints
	// write 1 to toggle, write 0 to leave as-is
	//
	//
	//Bits 13:12 STAT_RX [1:0]: Status bits, for reception transfers
	//	00: Disabled, all reception requests to this EP are ignored (default post-reset)
	//	01: Stall, EP is stalled and all reception requests will get STALL handshake
	//	10: NAK, all reception requests will get NAK handshake
	//	11: Valid, this endpoint is enabled for reception
	//	Hardware sets these bits to NAK after successful OUT/SETUP xfr
	//	Software can toggle these bits by writting a 1, writting 0 has no effect
	// USE: to enable the endpoint toggle these bits to set.  HW will clear bit12 when xfr is recieved.
	// 	That way subsequent xfrs will get NAK'd until software processes EP and ready's it for another.
	// 	Once processed toggle back to VALID so next xfr can occur
	// write 1 to toggle, write 0 to leave as-is
	//
	//
	//Bit 11 SETUP: Setup transaction completed
	//	This bit is read-only and indicates if last completed transaction was a SETUP.
	//	This bit only gets set for control endpoints.  It remains set while CTR_RX is set.
	// USE: For control EPs read this bit to determine if received xfr is SETUP or OUT.
	// writes have no effect, read only
	//
	//Bits 10:9 EP_TYPE[1:0]: Endpoint type
	//	00: BULK
	//	01: CONTROL
	//	10: ISO
	//	11: INTERRUPT
	//	EP0 must always be CONTROL and always available (set by address==0)
	// USE: set these bits to desired endpoint type.  Hardware uses these bits to dictate it's behavior.
	// bits are read/write, RMW to leave as is
	//
	//Bit 8 EP_KIND: Endpoint kind
	//	BULK: DBL_BUF, this bit is set by software to indicate double buffered EP
	//	CONTROL: STATUS_OUT, this bit is set by software to indicate that STATUS_OUT is expected.
	//		This will cause all OUT transactions with more than zero data bytes to be STALL'd instead of ACK.
	//		When clear, OUT transactions of any data length will be allowed.
	//	ISO/INT: unused
	// USE: set for BULK EPs if want to double buffer the endpoint
	// 	set for CONTROL EPs for robustness, when a STATUS_OUT is expected and want non-zero data to be STALL'd
	// 	Think we can get by ignoring this bit for now
	// bit is read/write, RMW to leave as-is
	// 	
	//Bit 7 CTR_TX: Correct Transfer for transmission
	//	Similar to CTR_RX, but for transmissions.  Hardware sets this bit on successful transmission completion.
	//	This won't get set if transfer failed.  Software can only write this bit to 0.
	// USE: read this bit after interrupt to determine if source of interrupt was due to successful transmission.
	// 	process what's needed for upcoming transmission if needed, and clear this bit to subsequent interrupts 
	// 	will be properly detected.
	// write 0 to clear, write 1 to leave as-is
	//
	//
	//Bit 6 DTOG_TX: Data Toggle, for transmission transfers
	//	Similar to DTOG_RX, but for transmissions.  Hardware toggles this bit after an ACK of data transmit.
	//	For control EPs HW sets this bit at reception of SETUP PID
	//	For double buffered, and iso EPs this bit has different functions
	//	Software can toggle this bit by writting a 1, writting 0 is ignored.  Required for non-control EPs
	// USE: should be able to ignore for control EPs.  
	// To leave as-is write a 0.
	//
	//Bits 5:4 STAT_TX [1:0]: Status bits, for transmission transfers
	//	00: Disabled, all transmission requests to this EP are ignored (default post-reset)
	//	01: Stall, EP is stalled and all transmission requests will get STALL handshake
	//	10: NAK, all transmission requests will get NAK handshake
	//	11: Valid, this endpoint is enabled for transmission
	//	Hardware changes these bits from Valid to NAK after successful transmission.
	//	That way subsequent transmit requests will be NAK'd until software can prepare the next one.
	//	These bits will toggle when wrote to with a 1, writting a 0 doesn't affect them.
	// USE: Toggle the bits to Valid when data has been loaded into buffer and ready for transmission.
	// write 1 to toggle, write 0 to leave as-is
	//
	//Bits 3:0 EA[3:0]: Endpoint address
	//	Software must write the address to these bits to enable the endpoint.
	// USE: set these bits to the "endpoint number" ie 0 for EP0, 1 for EP1, etc.
	// 	These default to 0, so post reset all USB_EPnR are set to be EP0.
	// 	But because nothing is initialized they'll all ignore anything going on.
	// bits are read/write, RMW to leave as is
	
	//Reset done, now setup as if USB reset has occured
	//USB reset (RESET interrupt)
	//When this event occurs, the USB peripheral is put in the same conditions it is left by the system 
	//reset after the initialization described in the previous paragraph: communication is disabled in all 
	//endpoint registers (the USB peripheral will not respond to any packet). 
	//As a response to the USB reset event, the USB function must be enabled, having as USB address 0, 
	//implementing only the default control endpoint (endpoint address is 0 too). 
	//This is accomplished by setting the Enable Function (EF) bit of the USB_DADDR register and 
	//initializing the EP0R register and its related packet buffers accordingly. 
	//
	//During USB enumeration process, the host assigns a unique address to this device, 
	//which must be written in the ADD[6:0] bits of the USB_DADDR register, and configures any other necessary endpoint.
	//When a RESET interrupt is received, the application software is responsible to enable again 
	//the default endpoint of USB function 0 within 10 ms from the end of reset sequence which triggered the interrupt.
	
	//clear any pending interrupts
	USB->ISTR = 0;
	
	//initialize EP specific register for EP0 as CONTROL and ready for RX of any OUT
	//assuming reset condition with all bits clear, except CTR_RX/TX holding prior to reset value
	USB->EP0R = (uint16_t) (USB_EP_RX_VALID | USB_EP_CONTROL | USB_EP_TX_NAK);
	//clears CTR_RX/TX bits, ready to recieve, transmit disabled, sets to control type, expect any out, set addr to EP0
	//
	


}


//Create pointer to 16bit array with 512 entries (1024Bytes)
//can only be accessed in byte or half words, not full 32bit words!
//uint16_t volatile (* const usb_buff)[512] = (void*)USB_PMAADDR; 
//this was suggestion by: http://a3f.at/articles/register-syntax-sugar
//which errors on compilation due to assigning of type array
uint16_t volatile (* const usb_buff) = (void*)USB_PMAADDR;


//buffer table itself is located in 1KB buffer above, but it's location is programmable
#define USB_BTABLE_BASE ((uint16_t) 0x0000)	//least 3 significant bits are forced to zero
#define USB_BTABLE_SIZE 64	//32x 16bit halfwords

//Endpoint 0: setup as 8Bytes TX & RX following buffer table
#define EP0_SIZE	0x08		//8Bytes same as usb 1.1 for now
#define EP0_TX_ADDR     (USB_BTABLE_BASE + USB_BTABLE_SIZE)
#define EP0_TX_BASE	(EP0_TX_ADDR / 2)
#define EP0_TX_SIZE	EP0_SIZE	//8Bytes

#define EP0_RX_ADDR     (EP0_TX_ADDR + EP0_TX_SIZE)
#define EP0_RX_BASE	(EP0_RX_ADDR / 2)
#define EP0_RX_SIZE	EP0_SIZE	//8Bytes
//#define LOG0     	(EP0_RX_ADDR + EP0_RX_SIZE)
#define LOG0     	(64+16) / 2
#define LOG4		LOG0 + 2
#define LOG8		LOG0 + 4
#define LOGC		LOG0 + 6
#define LOG10		LOG0 + 8


//Transmission buffer address n (USB_ADDRn_TX)
//Address offset: [USB_BTABLE] + n*8
//Note: In case of double-buffered or isochronous endpoints in the IN direction, this address location is referred to as USB_ADDRn_TX_0.
//In case of double-buffered or isochronous endpoints in the OUT direction, this address
//location is used for USB_ADDRn_RX_0.
#define USB_ADDR0_TX (USB_BTABLE_BASE + 0*8)
#define USB_ADDR1_TX (USB_BTABLE_BASE + 1*8)
#define USB_ADDR2_TX (USB_BTABLE_BASE + 2*8)
#define USB_ADDR3_TX (USB_BTABLE_BASE + 3*8)
#define USB_ADDR4_TX (USB_BTABLE_BASE + 4*8)
#define USB_ADDR5_TX (USB_BTABLE_BASE + 5*8)
#define USB_ADDR6_TX (USB_BTABLE_BASE + 6*8)
#define USB_ADDR7_TX (USB_BTABLE_BASE + 7*8)
//Bits 15:1 ADDRn_TX[15:1]: Transmission buffer address
//These bits point to the starting address of the packet buffer containing data to be transmitted
//by the endpoint associated with the USB_EPnR register at the next IN token addressed to it.
//Bit 0 Must always be written as ����0 since packet memory is half-word wide and all packet buffers
//must be half-word aligned.

//Transmission byte count n (USB_COUNTn_TX)
//Address offset: [USB_BTABLE] + n*8 + 2
//Note: In case of double-buffered or isochronous endpoints in the IN direction, this address location is referred to as USB_COUNTn_TX_0.
//In case of double-buffered or isochronous endpoints in the OUT direction, this address
//location is used for USB_COUNTn_RX_0.
#define USB_COUNT0_TX (USB_BTABLE_BASE + 0*8 + 1)
#define USB_COUNT1_TX (USB_BTABLE_BASE + 1*8 + 1)
#define USB_COUNT2_TX (USB_BTABLE_BASE + 2*8 + 1)
#define USB_COUNT3_TX (USB_BTABLE_BASE + 3*8 + 1)
#define USB_COUNT4_TX (USB_BTABLE_BASE + 4*8 + 1)
#define USB_COUNT5_TX (USB_BTABLE_BASE + 5*8 + 1)
#define USB_COUNT6_TX (USB_BTABLE_BASE + 6*8 + 1)
#define USB_COUNT7_TX (USB_BTABLE_BASE + 7*8 + 1)
//Bits 15:10These bits are not used since packet size is limited by USB specifications to 1023 bytes. Their
//value is not considered by the USB peripheral.
//Bits 9:0 COUNTn_TX[9:0]: Transmission byte count
//These bits contain the number of bytes to be transmitted by the endpoint associated with the
//USB_EPnR register at the next IN token addressed to it.


//Reception buffer address n (USB_ADDRn_RX)
//Address offset: [USB_BTABLE] + n*8 + 4
//Note: In case of double-buffered or isochronous endpoints in the OUT direction, this address
//location is referred to as USB_ADDRn_RX_1.
//In case of double-buffered or isochronous endpoints in the IN direction, this address location is used for USB_ADDRn_TX_1.
#define USB_ADDR0_RX (USB_BTABLE_BASE + 0*8 + 2)
#define USB_ADDR1_RX (USB_BTABLE_BASE + 1*8 + 2)
#define USB_ADDR2_RX (USB_BTABLE_BASE + 2*8 + 2)
#define USB_ADDR3_RX (USB_BTABLE_BASE + 3*8 + 2)
#define USB_ADDR4_RX (USB_BTABLE_BASE + 4*8 + 2)
#define USB_ADDR5_RX (USB_BTABLE_BASE + 5*8 + 2)
#define USB_ADDR6_RX (USB_BTABLE_BASE + 6*8 + 2)
#define USB_ADDR7_RX (USB_BTABLE_BASE + 7*8 + 2)
//Bits 15:1 ADDRn_RX[15:1]: Reception buffer address
//These bits point to the starting address of the packet buffer, which will contain the data
//received by the endpoint associated with the USB_EPnR register at the next OUT/SETUP
//token addressed to it.
//Bit 0 This bit must always be written as ����0 since packet memory is half-word wide and all packet
//buffers must be half-word aligned.


//Reception byte count n (USB_COUNTn_RX)
//Address offset: [USB_BTABLE] + n*8 + 6
//Note: In case of double-buffered or isochronous endpoints in the OUT direction, this address
//location is referred to as USB_COUNTn_RX_1.
//In case of double-buffered or isochronous endpoints in the IN direction, this address location is used for USB_COUNTn_TX_1.
#define USB_COUNT0_RX (USB_BTABLE_BASE + 0*8 + 3)
#define USB_COUNT1_RX (USB_BTABLE_BASE + 1*8 + 3)
#define USB_COUNT2_RX (USB_BTABLE_BASE + 2*8 + 3)
#define USB_COUNT3_RX (USB_BTABLE_BASE + 3*8 + 3)
#define USB_COUNT4_RX (USB_BTABLE_BASE + 4*8 + 3)
#define USB_COUNT5_RX (USB_BTABLE_BASE + 5*8 + 3)
#define USB_COUNT6_RX (USB_BTABLE_BASE + 6*8 + 3)
#define USB_COUNT7_RX (USB_BTABLE_BASE + 7*8 + 3)
//This table location is used to store two different values, both required during packet
//reception. The most significant bits contains the definition of allocated buffer size, to allow
//buffer overflow detection, while the least significant part of this location is written back by the
//USB peripheral at the end of reception to give the actual number of received bytes. Due to the 
//restrictions on the number of available bits, buffer size is represented using the number of allocated 
//memory blocks, where block size can be selected to choose the trade-off between fine-granularity/small-buffer 
//and coarse-granularity/large-buffer. The size of allocated buffer is a part of the endpoint descriptor 
//and it is normally defined during the enumeration process according to its maxPacketSize parameter value 
//(See ����Universal Serial Bus Specification����).
//
//Bit 15 BL_SIZE: Block size
//This bit selects the size of memory block used to define the allocated buffer area.
//	-If BL_SIZE=0, the memory block is 2-byte large, which is the minimum block
//	allowed in a half-word wide memory. With this block size the allocated buffer size
//	ranges from 2 to 62 bytes.
//	-If BL_SIZE=1, the memory block is 32-byte large, which allows to reach the
//	maximum packet length defined by USB specifications. With this block size the
//	allocated buffer size theoretically ranges from 32 to 1024 bytes, which is the longest
//	packet size allowed by USB standard specifications. However, the applicable size is limited by the available buffer memory.
#define	BL_SIZE32	(uint16_t) 0x8000
#define	BL_SIZE2	(uint16_t) 0x0000
#define	NUM_BLOCKS	10	//shift number of blocks by this amount
#define	RX_COUNT_MSK	(uint16_t) 0x03FF
//
//Bits 14:10 NUM_BLOCK[4:0]: Number of blocks
//These bits define the number of memory blocks allocated to this packet buffer. The actual amount of 
//allocated memory depends on the BL_SIZE value as illustrated in Table127.
//
//Bits 9:0 COUNTn_RX[9:0]: Reception byte count
//These bits contain the number of bytes received by the endpoint associated with the
//USB_EPnR register during the last OUT/SETUP transaction addressed to it.




//#define TSSOP20	//defined when using TSSOP-20 part to get PA11/12 alternate mapping to the pins

void init_usb()
{


	//initialize i/o
	// TSSOP-20: On STM32F070x6 devices, pin pair PA11/12 can be remapped instead of pin pair PA9/10 using SYSCFG_CFGR1 register.
#ifdef TSSOP20
	//by default PA9/10 are configured to pins 17/18, have to set bits to map PA11/12 which contain USB PHY
	SYSCFG->CFGR1 |= (uint32_t) PA11_PA12_RMP;
#endif
	//
	// ensure GPIO bank A is enabled after the discussion below, I'm not even sure this is needed..
	RCC->AHBENR |= (uint32_t) (1<<IOPAEN);
	// set GPIO alternate function to USB
	// Can't find the USB alternate function mapping on the datasheets!
	// Not sure how this is supposed to work..
	// Considering the USB PHY are terminated, they don't lend themselves well to the basic GPIO structure
	// which can be taken over by common alternate functions.  Wondering if the PHY are hardwired to the pins.
	// If that were the case, then really just need to make sure the USB GPIO are left floating inputs at reset/default.
	// Can't find anything in the datasheet that explains this so I'm left to assume there's nothing to do
	// As the only other thing I can do is take wild stabs in the dark as to what alternate function mapping is needed..
	// Considering the USB PHY has it's own power and clock supply, this logic seems reasonable enough
	

	//reset USB device and release, don't think this is needed, but I'm at a loss of what's wrong...
	RCC->APB1RSTR |= RCC_APB1RSTR_USBRST;	//reset
	RCC->APB1RSTR &= ~RCC_APB1RSTR_USBRST;	//release reset


	//Sections copied from reference manual
	//As a first step application software needs to activate register macrocell clock and de-assert 
	//macrocell specific reset signal using related control bits provided by device clock management logic.
	//
	//clock was already done in init_usb_clk function.
	//de-assert translates to set reset high I believe, but I'm still not sure I understand.
	//The next section talks about removing the reset condition with FRES/CNTR reg, but that can't be done yet.
	//So I think it's just covering bases to say the RCC_APB1RSTR register can't be set "in reset condition"
	//Actually I think this section of the datasheet is referring to how the first thing a host does once
	//a device is detected is send it a reset condition (lack of SOF packets) and as this code is being ran that
	//has yet to occur.  So we need to set things up to expect the first "USB command" to be recieved via the cable
	//as reset which equates to enabling the reset interrupt and having it call reset recovery routine.
	
	//After that, the analog part of the device related to the USB transceiver must be 
	//switched on using the PDWN bit in CNTR register, which requires a special handling. 
	USB->CNTR &= (uint16_t) ~USB_CNTR_PDWN; //default is set, clear to power up USB

	//This bit is intended to switch on the internal voltage references that supply the port transceiver. 
	//This circuit has a defined startup time (tSTARTUP specified in the datasheet) tSTARTUP = 1usec
	//during which the behavior of the USB transceiver is not defined. 
	int delay = 0;
	for( delay = 0; delay < 48; delay++ ){
		//16Mhz = 62.5nsec clock, 1usec = 1Mhz
		//need 16 clocks to delay 1usec this loop will take a few instruction cycles at least
		//Perhaps this code will be ran while the core is running at 48Mhz
		//So future proof by waiting 3 times as long
	}

	//It is thus necessary to wait this time, after setting the PDWN bit in the CNTR register, 
	//before removing the reset condition on the USB part (by clearing the FRES bit in the CNTR register). 
	USB->CNTR &= (uint16_t) ~USB_CNTR_FRES; //default set, clear to remove reset
	
	//Clearing the ISTR register then removes any spurious pending interrupt before any other macrocell operation is enabled.
	//Entire register is read, or clear by writing 0.
	USB->ISTR = (uint16_t) 0;

	//At system reset, the microcontroller must initialize all required registers and the packet buffer description table, 
	//to make the USB peripheral able to properly generate interrupts and data transfers. 
	//
	//Structure and usage of packet buffers
	//Each bidirectional endpoint may receive or transmit data from/to the host. The received data 
	//is stored in a dedicated memory buffer reserved for that endpoint, while another memory
	//buffer contains the data to be transmitted by the endpoint. Access to this memory is
	//performed by the packet buffer interface block, which delivers a memory access request
	//and waits for its acknowledgment. Since the packet buffer memory has to be accessed by
	//the microcontroller also, an arbitration logic takes care of the access conflicts, using half
	//APB cycle for microcontroller access and the remaining half for the USB peripheral access.
	//In this way, both the agents can operate as if the packet memory is a dual-port SRAM,
	//without being aware of any conflict even when the microcontroller is performing back-to-
	//back accesses. The USB peripheral logic uses a dedicated clock. The frequency of this
	//dedicated clock is fixed by the requirements of the USB standard at 48 MHz, and this can be 
	//different from the clock used for the interface to the APB bus. Different clock configurations 
	//are possible where the APB clock frequency can be higher or lower than the USB peripheral
	//one.
	//
	//Note: Due to USB data rate and packet memory interface requirements, the APB clock must have a minimum 
	//frequency of 10 MHz to avoid data overrun/underrun problems.
	//
	//Each endpoint is associated with two packet buffers (usually one for transmission and the
	//other one for reception). Buffers can be placed anywhere inside the packet memory
	//because their location and size is specified in a buffer description table, which is also
	//located in the packet memory at the address indicated by the USB_BTABLE register. 
	//
	//Each table entry is associated to an endpoint register and it is composed of four 16-bit half-words
	//so that table start address must always be aligned to an 8-byte boundary (the lowest three
	//bits of USB_BTABLE register are always "000"). Buffer descriptor table entries are
	//described in the Section30.6.2: Buffer descriptor table. If an endpoint is unidirectional and it
	//is neither an Isochronous nor a double-buffered bulk, only one packet buffer is required 
	//(the one related to the supported transfer direction). Other table locations related to unsupported 
	//transfer directions or unused endpoints, are available to the user. Isochronous and double-
	//buffered bulk endpoints have special handling of packet buffers (Refer to Section 30.5.4:
	//Isochronous transfers and Section 30.5.3: Double-buffered endpoints respectively). The
	//relationship between buffer description table entries and packet buffer areas is depicted in Figure323.
	//
	//Each packet buffer is used either during reception or transmission starting from the bottom.
	//The USB peripheral will never change the contents of memory locations adjacent to the
	//allocated memory buffers; if a packet bigger than the allocated buffer length is received
	//(buffer overrun condition) the data will be copied to the memory only up to the last available
	//location.
	//
	//Buffer descriptor table
	//Although the buffer descriptor table is located inside the packet buffer memory, its entries
	//can be considered as additional registers used to configure the location and size of the
	//packet buffers used to exchange data between the USB macro cell and the device.
	//The first packet memory location is located at 0x40006000. The buffer descriptor table
	//entry associated with the USB_EPnR registers is described below. The packet memory
	//should be accessed only by byte (8-bit) or half-word (16-bit) accesses. Word (32-bit) accesses are not allowed.
	//
	//Buffer table address (USB_BTABLE) this is the address of the buffer table which is contained in the 1KB of RAM itself
	//By default this value is set to zero, that's what I would have choosen anyway, so doesn't need initialized
	//But let's go ahead and do it so it's easy to move later if needed and gives us defines to use for addressing
	USB->BTABLE = USB_BTABLE_BASE;

	
	//Endpoint initialization
	//The first step to initialize an endpoint is to write appropriate values to the ADDRn_TX/ADDRn_RX registers 
	//so that the USB peripheral finds the data to be transmitted already available and the data to be received can be buffered. 
	//The EP_TYPE bits in the USB_EPnR register must be set according to the endpoint type, eventually using 
	//the EP_KIND bit to enable any special required feature. 
	//On the transmit side, the endpoint must be enabled using the STAT_TX bits in the USB_EPnR register 
	//and COUNTn_TX must be initialized. 
	//For reception, STAT_RX bits must be set to enable reception and COUNTn_RX must be written with 
	//the allocated buffer size using the BL_SIZE and NUM_BLOCK fields.
	
	//only setup endpoint zero buffers for now that's all that's required to satisfy USB and get started
	usb_buff[USB_ADDR0_TX] = EP0_TX_ADDR;
	//TX count is set to the number of bytes to xfr in next packet
	//usb_buff[USB_COUNT0_TX] = EP0_TX_SIZE;
	usb_buff[USB_ADDR0_RX] = EP0_RX_ADDR;
	//RX count is made up of 3 parts
	//the MSB is block size 0->2Bytes, 1->32Bytes
	//bit 14:10 is number of blocks
	//end point size = block size * num blocks
	//bits 9:0 are set by USB block based on actual count received
	usb_buff[USB_COUNT0_RX] = (uint16_t) ((BL_SIZE2) | ((EP0_RX_SIZE/2)<<NUM_BLOCKS)) ;	//set EP0 to 8 bytes


	//Clear buffers for debug purposes
//	usb_buff[EP0_TX_BASE] = (uint16_t) 0x1111;
//	usb_buff[EP0_TX_BASE+1] = (uint16_t) 0x2222;
//	usb_buff[EP0_TX_BASE+2] = (uint16_t) 0x3333;
//	usb_buff[EP0_TX_BASE+3] = (uint16_t) 0x4444;
//
//	usb_buff[EP0_RX_BASE] = (uint16_t) 0x5555;
//	usb_buff[EP0_RX_BASE+1] = (uint16_t) 0x6666;
//	usb_buff[EP0_RX_BASE+2] = (uint16_t) 0x7777;
//	usb_buff[EP0_RX_BASE+3] = (uint16_t) 0x8888;


	//All registers not specific to any endpoint must be initialized according to the needs of application software 
	//(choice of enabled interrupts, chosen address of packet buffers, etc.). 
	//Then the process continues as for the USB reset case (see further paragraph).
	
	
	//initialize all registers not specific to any endpoint
	//
	//LPM control and status register (USB_LPMCSR)
	//Link power management, this is disabled by default, lets leave it that way.
	

	
	//now all the registers and buffers are setup so endpoint specific registers can be set
	usb_reset_recovery();

	NVIC_EnableIRQ( USB_IRQn );

	//enable interrupts
	//USB control register (USB_CNTR)
	//only enable interrupts for correct transfers for now
//	USB->CNTR |= (USB_CNTR_CTRM | USB_CNTR_PMAOVRM | USB_CNTR_ERRM | USB_CNTR_WKUPM |
//			USB_CNTR_SUSPM | USB_CNTR_RESETM | USB_CNTR_SOFM | USB_CNTR_ESOFM | USB_CNTR_L1REQM ); 	
	USB->CNTR |= ( USB_CNTR_RESETM );
	//USB control register (USB_ISTR) will indicate what endpoint and direction created the interrupt
	//Apparently you don't even need to set the CTRM interrupt as it's not of much use
	//The EP0R register creates it's own interrupt which is always enabled upon completion of transfer
	//The ITSR is still useful for determining which EP and dir of transfer that occured.
	//Having the reset interrupt is vital as apparently the first thing that occurs over the USB bus is a reset.
	//Not sure if that's caused by the host intentionally, or just the nature of hot plugging where some
	//SOF's are missed during the mating of the connectors or what.  Banged my head for awhile till actually
	//serviced the reset interrupt as needed
	
	//Battery charging detector (USB_BCDR)
	//Bit 15 DPPU: DP pull-up control
	//This bit is set by software to enable the embedded pull-up on the DP line. Clearing it to ����0����
	//can be used to signalize disconnect to the host when needed by the user software.
	//all other bits have to do with battery charging which is off by default
	//
	//Enable the data plus pull up resistor to signal to host that USB device is connected
	USB->BCDR = USB_BCDR_DPPU;
}

const uint8_t device_desc[18] = {
//Device Descriptor
// Offset 	Field 			Size(B)	Value 		Description
// 0 	bLength 		1 	Number 		Size of the Descriptor in Bytes (18 bytes)
					18,
// 1 	bDescriptorType 	1 	Constant 	Device Descriptor (0x01)
					0x01,
// 2 	bcdUSB 			2 	BCD 		USB Specification Number which device complies too.
					0x00, 0x02,
// 4 	bDeviceClass 		1 	Class 		Class Code (Assigned by USB Org) 
					0xFF,
// 							If equal to Zero, each interface specifies it����s own class code 
// 							If equal to 0xFF, the class code is vendor specified.  
// 							Otherwise field is valid Class Code.
// 5 	bDeviceSubClass 	1 	SubClass 	Subclass Code (Assigned by USB Org)
					0x00,
// 6 	bDeviceProtocol 	1 	Protocol 	Protocol Code (Assigned by USB Org)
					0x00,
// 7 	bMaxPacketSize 		1 	Number 		Maximum Packet Size for Zero Endpoint. Valid Sizes are 8, 16, 32, 64 (for usb 2.0)
					EP0_SIZE,
// 8 	idVendor 		2 	ID 		Vendor ID (Assigned by USB Org)
					0xC0, 0x16,
// 10 	idProduct 		2 	ID 		Product ID (Assigned by Manufacturer)
					0xDC, 0x05,
// 12 	bcdDevice 		2 	BCD 		Device Release Number
					0x00, 0x02,
// 14 	iManufacturer 		1 	Index 		Index of Manufacturer String Descriptor
					0x01,
// 15 	iProduct 		1 	Index 		Index of Product String Descriptor
					0x02,
// 16 	iSerialNumber 		1 	Index 		Index of Serial Number String Descriptor
					0x00,
// 17 	bNumConfigurations 	1 	Integer 	Number of Possible Configurations
					0x01 };


//	The bcdUSB field reports the highest version of USB the device supports. The value is in binary coded decimal 
//	with a format of 0xJJMN where JJ is the major version number, M is the minor version number and N is the sub minor 
//	version number. e.g. USB 2.0 is reported as 0x0200, USB 1.1 as 0x0110 and USB 1.0 as 0x0100.
//
//	The bDeviceClass, bDeviceSubClass and bDeviceProtocol are used by the operating system to find a class driver 
//	for your device. Typically only the bDeviceClass is set at the device level. Most class specifications choose to 
//	identify itself at the interface level and as a result set the bDeviceClass as 0x00. This allows for the one 
//	device to support multiple classes.
//
//	The bMaxPacketSize field reports the maximum packet size for endpoint zero. All devices must support endpoint zero.
//
//	The idVendor and idProduct are used by the operating system to find a driver for your device. The Vendor ID is assigned by the USB-IF.
//
//	The bcdDevice has the same format than the bcdUSB and is used to provide a device version number. This value is assigned by the developer.
//
//	Three string descriptors exist to provide details of the manufacturer, product and serial number. There is no 
//	requirement to have string descriptors. If no string descriptor is present, a index of zero should be used.
//
//	bNumConfigurations defines the number of configurations the device supports at its current speed.

//#define __packed __attribute((packed))__
typedef struct usbRequest_t{
	uint8_t		bmRequestType;
	uint8_t		bRequest;
	uint16_t	wValue;
	uint16_t	wIndex;
	uint16_t	wLength;
}usbRequest_t;
//}__attribute((__packed__))usbRequest_t;

//	bmRequestType
//D7 Data Phase Transfer Direction
//0 = Host to Device
//1 = Device to Host
#define REQ_DIR		0x80
#define REQ_DIR_2DEV	0x00
#define REQ_DIR_2HOST	0x80
//D6..5 Type
//0 = Standard
//1 = Class
//2 = Vendor
//3 = Reserved
#define REQ_TYPE	0x60
#define REQ_TYPE_STD	0x00
#define REQ_TYPE_CLASS	0x20
#define REQ_TYPE_VEND	0x40
#define REQ_TYPE_RES	0x60
//D4..0 Recipient
//0 = Device
//1 = Interface
//2 = Endpoint
//3 = Other
#define REQ_RECIP	0x0F
#define REQ_RECIP_DEV	0x00
#define REQ_RECIP_INT	0x01
#define REQ_RECIP_EP	0x02
#define REQ_RECIP_OTH	0x03
//4..31 = Reserved

static uint16_t num_bytes_req;
static uint16_t num_bytes_sending;
static uint16_t num_bytes_sent;
static uint16_t *return_in_data;

//function gets called after reception of setup packet for IN transfer,
//and each time an interrupt for successful data tx from control EP
//So it's as if this function gets called *AFTER* each transmission
//Therefore it's task is to prep the next transmission should there be one needed
//If we don't need another transmission, prepare to receive status from host
//currently hardcoded for EP0 only
//num_bytes_sending, num_bytes_sent, and *return_in_data must be initialized prior to entry

void control_xfr_in(){
	//determine if have data remaining to be sent
	
	if ( num_bytes_sent == num_bytes_sending ) {
		//no more data to send
		//wrap up transfer and prep endpoint for next setup packet
		//
		//While enabling the last data stage, the opposite direction should be set to NAK, so that, if
		//the host reverses the transfer direction (to perform the status stage) immediately, it is kept
		//waiting for the completion of the control operation. If the control operation completes
		//successfully, the software will change NAK to VALID, otherwise to STALL. At the same time,
		//if the status stage will be an OUT, the STATUS_OUT (EP_KIND in the USB_EPnR register)
		//bit should be set, so that an error is generated if a status transaction is performed with not-
		//zero data. When the status transaction is serviced, the application clears the STATUS_OUT
		//bit and sets STAT_RX to VALID (to accept a new command) and STAT_TX to NAK (to delay
		//a possible status stage immediately following the next setup).

		return;
	}
	
	//if more data than fits in EP0 copy over 8bytes
	if ( num_bytes_sent+8 <= num_bytes_sending ) {
		//copy data into EP0 buffer table ram
		//usb buffer ram is only accessible in halfwords/bytes (16/8bits)
		usb_buff[EP0_TX_BASE] 	= return_in_data[num_bytes_sent/2];
		num_bytes_sent += 2;
		usb_buff[EP0_TX_BASE+1] = return_in_data[num_bytes_sent/2];
		num_bytes_sent += 2;
		usb_buff[EP0_TX_BASE+2] = return_in_data[num_bytes_sent/2];
		num_bytes_sent += 2;
		usb_buff[EP0_TX_BASE+3] = return_in_data[num_bytes_sent/2];
		num_bytes_sent += 2;

		usb_buff[USB_COUNT0_TX] = EP0_TX_SIZE;

	} else {// if ( num_bytes_sent < num_bytes_sending ) {
	
		usb_buff[USB_COUNT0_TX] = num_bytes_sending - num_bytes_sent;
	
		//TODO fix this hack junk to handle final data packets less than 8 Bytes
		usb_buff[EP0_TX_BASE] 	= return_in_data[num_bytes_sent/2];
		num_bytes_sent += 2;
		usb_buff[EP0_TX_BASE+1] = return_in_data[num_bytes_sent/2];
		num_bytes_sent += 2;
		usb_buff[EP0_TX_BASE+2] = return_in_data[num_bytes_sent/2];
		num_bytes_sent += 2;
		usb_buff[EP0_TX_BASE+3] = return_in_data[num_bytes_sent/2];
		num_bytes_sent += 2;
		num_bytes_sent = num_bytes_sending;

	}

#define USB_EP_TX_NAK_TO_VALID	USB_EP_TX_STALL
	//setup EP0R for transmit
	//to keep from affecting any bits one must:
	//RMW the r/w fields, write 0 to toggle fields, write 1 to ctr bits
	USB_EP0R_TX_VALID();
//#define	USB_EP0R_TX_VALID() USB->EP0R = ((((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX)) & (USB_EPREG_MASK | USB_EPTX_STAT)) ^ USB_EPTX_STAT )
//	USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) 	//set rx/tx field to keep from accidentally clearing
//			& USB_EPREG_MASK ) 	//mask out toggle feilds making them zero
//TODO fix assumption that we only need to go from TX_NAK to TX_VALID
//			| USB_EP_TX_NAK_TO_VALID );	//set bit that would like to toggle
	return;
}

void control_xfr_out(){}

uint16_t standard_req_in( usbRequest_t *spacket ){

	//just return device descriptor for now
	return_in_data = (uint16_t *)device_desc;
	return 18;


}

//USB IRQ handler calls this function after recieving a control setup packet
//function is responsible for preparing follow on data/status transfers to complete control xfr
//must set everything up for control_xfr_in/out functions to be called during data packets
void control_xfr_init( usbRequest_t *spacket ) {

	//determine number of requested data payload bytes
	num_bytes_req = spacket->wLength;
	num_bytes_sent = 0;

	//Vusb calls usbFunctionSetup which sets usbMsgPtr to point to IN data array
	//and returns length of data to be returned.
	//Vusb needs nothing else for IN transfers as it already has the data
	//Vusb calls usbFunctionWrite for OUT xfr on subsequent of data packet arrival (8bytes max)

	//if data is IN (tx) need to aquire pointer to data that we'd like to transmit
	//don't think we'll get a zero data length IN as that would mean the host sends all packets
	//I suppose this is possible for poorly written host application..?
	//Vusb had option to provide function that would be called for each IN data packet
	//I found that to be slow and best avoided, so not going to bother supporting that for now..
	//Also need to get amount of data that the device would like to return, possible the
	//device has less data to send back than requested in wLength.
	
	
	//if ( spacket->bmRequestType & REQ_DIR == REQ_DIR_2HOST ) {
	if ( spacket->bmRequestType & REQ_DIR ) {
		//IN transfer Host wants us to send data (tx)
		switch ( spacket->bmRequestType & REQ_TYPE ) {
			case REQ_TYPE_STD:
				num_bytes_sending = standard_req_in( spacket );
				break;
			case REQ_TYPE_CLASS:
				num_bytes_sending = 0;//class_req_in( spacket );
				break;
			case REQ_TYPE_VEND:
				num_bytes_sending = 0;//vendor_req_in( spacket );
				break;
			case REQ_TYPE_RES:
			default:
				num_bytes_sending = 0;
				break;
		}
		//A USB device can determine the number and
		//direction of data stages by interpreting the data transferred in the SETUP stage, and is
		//required to STALL the transaction in the case of errors. To do so, at all data stages before
		//the last, the unused direction should be set to STALL, so that, if the host reverses the
		//transfer direction too soon, it gets a STALL as a status stage.
		//
		//WRONG!!!  This ended up being a shot in the foot.  The VERY first request from the host is a 
		//SETUP device descriptor request for 16KBytes worth of data!!  
		//HOWEVER, the host tells us the device to shut it's mouth after the first data stage as if 
		//it doesn't want any more info by sending a STATUS state (OUT xfr with zero data)
		//So we need to be ready for this condition and be prepared to recieve a zero data "shutup" command
		//
		//Need to set RX to stall it should be NAK right now
#define USB_EP_RX_NAK_TO_STALL	USB_EPRX_STAT
		//to keep from affecting any bits one must:
		//RMW the r/w fields, write 0 to toggle fields, write 1 to ctr bits
		//then set any bits we'd like to toggle
		USB_EP0R_RX_VALID();
//#define	USB_EP0R_RX_VALID() USB->EP0R = ((((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX)) & (USB_EPREG_MASK | USB_EPRX_STAT)) ^ USB_EPRX_STAT )
//		USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX | USB_EP_CTR_TX) 	//set rx/tx field to keep from accidentally clearing
//				& USB_EPREG_MASK ) 	//mask out toggle feilds making them zero
//TODO fix assumption that we only need to go from TX_NAK to TX_VALID
//				| USB_EP_RX_NAK_TO_STALL );	//set bit(s) that would like to toggle

		//Now we've got the pointer to return data initialized
		//and number of bytes the firmware would like to return
		control_xfr_in();
		
	} else { 
		//OUT transfer Host is sending us data (rx)
		//if data is OUT (rx) need a function pointer of what to call when data is received
		//or if there is no data stage, we just need to send status packet

	}

}




//This is the ISR that gets called for USB interrupts
	//At the end of the transaction, an endpoint-specific interrupt is generated, reading status registers 
	//and/or using different interrupt response routines. The microcontroller can determine:
	// -which endpoint has to be served,
	// -which type of transaction took place, if errors occurred (bit stuffing, format, CRC,
	//	protocol, missing ACK, over/underrun, etc.).
	
	//USB interrupt status register (USB_ISTR)
	//This register contains the status of all the interrupt sources allowing application 
	//software to determine, which events caused an interrupt request.
void USB_IRQHandler(void)
{

	//all interrupts enabled by USB_CNTR, plus any successful rx/tx to an endpoint triggers this ISR
	
	//should be our goal to get out of this ISR asap, signal the transaction to the main thread
	//and let the main thread process the data.
	//That isn't really how V-usb is setup which we're looking to replace and be compatible with for starters.
	//So for now this isn't going to be setup ideally, but if USB is our top priority then maybe it's not so bad..

	//Plan is to determine source of interrupt
	//Service what's needed for the interrupt
	//Clear the pending/active interrupts which have been serviced
	//If there are other interrupts occuring simulateously we don't have to catch em all
	//	they'll trigger this ISR again
	//Return from interrupt
	
	//First check for successful transfer of USB packets
	//High level we can determine this by USB_ISTR:
	//	CTR:set if correct transfer occured (read only, cleared via the EP)
	//	DIR: 0-TX, 1-RX
	//	EP_ID[3:0]: endpoint that triggered the interrupt with following priority:
	//		-isosync & double-buffered bulk are considered first getting hi pri
	//		-then EPnR register number ie EP0R gets priority over EP2R
	//		NOTE: you can put whatever endpoint number in whichever register you choose
	//		endpoint0 doesn't have to go in EP0R register, the endpoint number is programmable!
	//		would probably make more sense if they used letters to define EPnR registers..
	//Low level we don't have to use EP_ID's priority assignments we could just check EPnR CTR_RX/TX bits
	//Kinda like EP_ID though as it allows us to focus on a single EP for the given ISR call
	
	//CAUTION!!!  usb_buff data can only be accessed in half words, or bytes.  No 32bit accesses!!!
	
	usbRequest_t *setup_packet;
	uint8_t	endpoint_num;
	

	//only have EP0 for now
	
	//check for OUT/SETUP
	if ( USB->EP0R & USB_EP_CTR_RX ) {
		log ++;
		if ( log >2) {
DEBUG_HI(); DEBUG_LO();
		}
		//clear RX interrupt leave everything else unaffected
		EP0R_CLR_CTR_RX();
//		USB->EP0R = (((USB->EP0R | USB_EP_CTR_TX) 	//set tx field to keep from accidentally clearing
//				& USB_EPREG_MASK ) 	//mask out toggle feilds making them zero
//				& ~USB_EP_CTR_RX );		//clear rx bit removing active interrupt
		//Note: clearing CTR_RX on a control EP, is enough for another setup packet to be accepted
		//this is true even if STAT_RX is STALL/NAK, so if the data needs retained it must be copied
		//elsewhere, or set STAT_RX to DISABLED to keep it from being stomped by the next setup

		if ( USB->EP0R & USB_EP_SETUP ) { //SETUP packet
//usb_buff[LOG0] = USB->EP0R;
			//set pointer to usb buffer, type ensures 8/16bit accesses to usb_buff
			setup_packet = (void*) &usb_buff[EP0_RX_BASE];
			control_xfr_init( setup_packet );
			
		} else { //OUT packet
		//number of bytes received is denoted in USB_COUNTn_RX buffer table
			control_xfr_out();
		}
	

	} else if ( USB->EP0R & USB_EP_CTR_TX ) {
//DEBUG_HI(); DEBUG_LO();
	//	log ++;
		//IN packet
			//LED_ON();
	//	if (log >= 2) {
	//		LED_ON();
	//	}
		//Servicing of the CTR_TX event starts clearing the interrupt bit; 

//usb_buff[LOG4] = USB->EP0R;
		//clear TX interrupt
		EP0R_CLR_CTR_TX();
		//USB->EP0R = (((USB->EP0R | USB_EP_CTR_RX) 	//set rx field to keep from accidentally clearing
		//		& USB_EPREG_MASK ) 	//mask out toggle feilds making them zero
		//		& ~USB_EP_CTR_TX );		//clear tx bit removing active interrupt

//		LED_ON();
//usb_buff[LOG8] = USB->EP0R;

		//the application software then prepares another buffer full of data to be sent, 
		//updates the COUNTn_TX table location with the number of byte to be transmitted during the next transfer, 
		//and finally sets STAT_TX to 11 (VALID) to re-enable transmissions. 
		control_xfr_in();

//usb_buff[LOGC] = USB->EP0R;
//usb_buff[LOG10] = log;
		//While the STAT_TX bits are equal to 10 (NAK), any IN request addressed to that endpoint is NAKed, 
		//indicating a flow control condition: the USB host will retry the transaction until it succeeds. 
		//It is mandatory to execute the sequence of operations in the above mentioned order to avoid losing the 
		//notification of a second IN transaction addressed to the same endpoint immediately following the one 
		//which triggered the CTR interrupt. 

		//I wasn't doing things in this order thinking that explains why I can't get a second transfer out

	}

	if ( USB->ISTR & USB_ISTR_RESET ) {
		//Anytime a reset condition occurs, the EPnR registers are cleared
		//Must re-enable the USB function and setup EP0 after any reset condition within 10msec
		usb_reset_recovery();
		//handled in reset recovery USB->ISTR &= ~USB_ISTR_RESET;
//DEBUG_HI(); DEBUG_LO();
	}
	
}

void init_led()
{
	//Need to supply clock to i/o port before anything can be done
	RCC->AHBENR |= ((1<<IOP_LED_EN));// | (1<<IOPCEN));
	RCC->AHBENR |= ((1<<IOP_DEBUG_EN));// | (1<<IOPCEN));
	
	//after reset most io registers are reset with exception of SWDIO/SWCLK for debugger
	//This makes all other pins Input, Pushpull (no effect till set as output), Slow, no pullup/down, output reg set low.
	//post reset, you only need to set the bits needed to set to desired config

	//SET LEDs as outputs Mode register is 2bits per pin, start as off
	//0b01 is output 2 bits per pin, 16 pins per port, fills entire 32bit MODER register
	//need to enable pins 9-6
	GPIO_LED->MODER |= (GPIO_Mode_OUT<<(LED*2));
	GPIO_DEBUG->MODER |= (GPIO_Mode_OUT<<(DEBUG*2));
	
	//LED_ON();
	//LED_OFF();

	//Switch already set to input, just enable the pullup
//	GPIOA->PUPDR |= (GPIO_PuPd_UP<<(SWITCH*2));

}

void main()
{
	//System is running at reset defaults
	
	//Default clock is in operation
	//Change system clock as needed
	init_clock();
	
	//Initialize periphery clocks as needed
	init_usb_clock();
	
	//Initialize interupts
	
	//Initialize WDT, core features, etc
	
	//enable interrupts
//	__enable_irq();		//clear's processor PRIMASK register bit to allow interrupts to be taken
	//Each interrupt must be enabled in the processors NVIC registers before the processor will activate pending interrupts
	//CMSIS interrupt control function 				Description
	//void NVIC_EnableIRQ(IRQn_t IRQn)				Enable IRQn
	//void NVIC_DisableIRQ(IRQn_t IRQn) 				Disable IRQn
	//uint32_t NVIC_GetPendingIRQ (IRQn_t IRQn) 			Return true (1) if IRQn is pending
	//void NVIC_SetPendingIRQ (IRQn_t IRQn) 			Set IRQn pending
	//void NVIC_ClearPendingIRQ (IRQn_t IRQn) 			Clear IRQn pending status
	//void NVIC_SetPriority (IRQn_t IRQn, uint32_t priority)	Set priority for IRQn
	//uint32_t NVIC_GetPriority (IRQn_t IRQn) 			Read priority of IRQn
	//void NVIC_SystemReset (void) 					Reset the system
	
	//default priority value is zero, values 0-192 in increments of 64 are possible
	//lower priority values are prioritized over higher values
	//if two interrupts have same priority value, the lower IRQ number is prioritized
	
	//Initialize io, periphery, etc
	//setup LED's as outputs and turn them on
	//setup user switch as input
	init_led();

	init_usb();

	
	//Initialize board/system
	
	uint32_t i = 0;

	//red LED on
//	GPIOC->ODR |= 1<<RED;
	
	//main infinite loop
	while(1) {

		i++;
		//to start i is zero and doesn't satisfy any checks below so all LEDs are off

		//once i reaches > 2M set output data register to i
		//This will make all LEDs turn on but not full brightness
		//they "flicker" as i iterates
	//at 8Mhz and zero flash wait states, this loops every ~6.7sec
	//at 8Mhz and  one flash wait states, this loops every ~10.2sec
		if ( i == 2000000 ) {
			//mask out all but the GPIO bits
			//a bit unfinished as we're writing to the entire GPIO register...
//			GPIOC->ODR = (i & 0x0000FFFF);
		//	GPIOC->ODR &= ~(1<<RED);
		//	LED_OFF();
			//log = 0xDEADBEEF;
	
		}

		if ( i == 3000000 ) {
			//manually set USB IRQ to pending, ensuring ISR works
			//NVIC_SetPendingIRQ ( USB_IRQn); 
			//log = 0xBEEFDEAD;
		}
		
		//once i > 4M set some LEDs on solid
		if ( i > 4000000 ) {
			i = 0;
			//log = 0xBEEFFEED;
			//GPIOC->ODR = 0x0000AAAA;	//blue green
			//GPIOC->ODR = 0x00005555;	//red orange
			//GPIOC->ODR = 1<<GREEN;
		//	GPIOC->ODR |= 1<<RED;
//			LED_ON();			//default 8Mhz HSI, flashes ~4.5sec
	
//			GPIOC->ODR &= ~(1<<YELLOW);

		}

		/*
		//once i > 8M reset back to 2M
		if ( i > 8000000 ) {
			i = 2000000;
		}

		//read in user switch
		if ( GPIOA->IDR & (1<<SWITCH) ) {
			//button pressed
			i = 0;	//reset the "counter"
			//GPIOC->ODR = 0x00000000;	//all off
			//GPIOC->ODR |= 1<<BLUE;
			GPIOC->ODR |= 1<<YELLOW;

		} else {
			//button not pressed
		}
		*/


	}
}
